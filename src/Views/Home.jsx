import React from 'react';
import { Link } from 'react-router-dom';
import Nav from '../components/Nav';
import Editor from '../components/Editor';
import Output from '../components/Output';

export default class Home extends React.Component {
	state = {
		color: '#ABB8C3',
		width: '25',
		height: '25',
		radius: '0',
	}

	render() {
		return (
			<div className="container">
				<Nav>
					<Link to="/gallery">Gallery</Link>
				</Nav>

				<div className="container__content">
					<Editor
						updateState={this._updateState}
						updateColor={this._updateColor}
						saveRect={this._saveRect}
						{...this.state} />
					<Output {...this.state} />
				</div>
			</div>
		)
	}

	/**
	 * @name _updateColor
	 * @desc update color when color picker triggers change
	 * @param {Object}
	 */
	_updateColor = (color) => {
		this.setState({
			color: color.hex
		})
	}

	/**
	 * @name _updateState
	 * @desc update state of Home component,
	 * if value larger than 300 set it to max value
	 * if value smaller than 10, seti it to min value
	 * @param {String, event}
	 */
	_updateState = (key, e) => {
		let value = Number(e.target.value);
		if (value > 300) value = 300;
		if (value < 10) value = 10;
		this.setState({
			[key]: JSON.stringify(value)
		})
	}

	/**
	 * @name _saveRect
	 * @desc save rectangle with given params and reset to default
	 * @param {event}
	 */
	_saveRect = (e) => {
		e.preventDefault();

		// check if there's item named `gallery` in localStorage, if not use empty array
		const gallery = localStorage.getItem('gallery') ?
			JSON.parse(localStorage.getItem('gallery')) :
			[];
		const rect = Object.assign({}, this.state);
		gallery.push(rect);
		localStorage.setItem('gallery', JSON.stringify(gallery));

		this.setState({
			color: '#ABB8C3',
			width: '25',
			height: '25',
			radius: '0',
		});
	}
}
