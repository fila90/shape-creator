import React from 'react';
import PropTypes from 'prop-types';

const Output = ({ color, width, height, radius }) => {
	return (
		<div className="output">
			<div style={{
				width: `${width}px`,
				height: `${height}px`,
				backgroundColor: `${color}`,
				borderRadius: `${radius}%`,
			}}></div>
		</div>
	)
}

Output.propTypes = {
	color: PropTypes.string.isRequired,
	width: PropTypes.string.isRequired,
	height: PropTypes.string.isRequired,
	radius: PropTypes.string.isRequired,
}

export default Output
