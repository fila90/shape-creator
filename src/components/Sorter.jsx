import React from 'react';
import PropTypes from 'prop-types';

const Sorter = ({ handleSelect }) => {
	return (
		<select
			onChange={handleSelect}
			className="sorter">
			<option value="">-- Select --</option>
			<option value="width">Width</option>
			<option value="height">Height</option>
		</select>
	)
}

Sorter.propTypes = {
	handleSelect: PropTypes.func.isRequired
};

export default Sorter
