import React from 'react';
import { shallow } from 'enzyme';
import Output from './Output';

describe('check if renders correctly', () => {
	test('test if renders', () => {
		const props = { color: "#f78da7", width: "170", height: "250", radius: "11" };
		const wrapper = shallow(<Output {...props} />);

		expect(wrapper.find('div').at(1).prop('style').backgroundColor).toEqual(props.color);
		expect(wrapper.find('div').at(1).prop('style').width).toEqual(`${props.width}px`);
		expect(wrapper.find('div').at(1).prop('style').height).toEqual(`${props.height}px`);
		expect(wrapper.find('div').at(1).prop('style').borderRadius).toEqual(`${props.radius}%`);
	})
})
