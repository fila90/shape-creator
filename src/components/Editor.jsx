import React from 'react';
import PropTypes from 'prop-types';
import { ChromePicker } from 'react-color';

const Editor = ({ updateState, updateColor, saveRect, color, width, height, radius }) => {

	/**
	* @name selectValue
	* @desc selevt value of input field on fovus, just for better UX
	* @param {event}
	*/
	const selectValue = (e) => {
		e.target.select();
	}

	return (
    <div className="editor">
      <ChromePicker width={'100%'}
                    disableAlpha={true}
                    onChangeComplete={updateColor}
                    color={color} />
      <form className="form"
            onSubmit={saveRect}>
        <p>Width</p>
        <label className="form__label">
          <input type="number"
                className="form__input"
                value={width}
                min="10"
                max="300"
                onFocus={selectValue}
                onChange={updateState.bind(this, 'width'
                )} />
          <input type="range"
                className="form__input form__input--large"
                value={width}
                min="10"
                max="300"
                onChange={updateState.bind(this, 'width'
                )} />
        </label>
        <p>Height</p>
        <label className="form__label">
          <input type="number"
                className="form__input"
                value={height}
                min="10"
                max="300"
                onFocus={selectValue}
                onChange={updateState.bind(this, 'height'
                )} />
          <input type="range"
                className="form__input form__input--large"
                value={height}
                min="10"
                max="300"
                onChange={updateState.bind(this, 'height'
                )} />
        </label>
        <p>Radius</p>
        <label>
          <input type="range"
                className="form__input form__input--large"
                value={radius}
                min="0"
                max="50"
                onChange={updateState.bind(this, 'radius'
                )} />
        </label>
        <button type="submit"
                className="form__btn">Save</button>
      </form>
    </div>
	)
}

Editor.propTypes = {
	updateState: PropTypes.func.isRequired,
	updateColor: PropTypes.func.isRequired,
	saveRect: PropTypes.func.isRequired,
	color: PropTypes.string.isRequired,
	width: PropTypes.string.isRequired,
	height: PropTypes.string.isRequired,
	radius: PropTypes.string.isRequired,
}

export default Editor
