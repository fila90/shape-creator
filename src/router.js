import React from 'react';
import {
	BrowserRouter as Router,
	Route,
} from 'react-router-dom';

import Home from './Views/Home';
import Gallery from './Views/Gallery';

const MainRouter = () => {
	return (
		<Router>
			<main className="app">
				<Route exact path="/" component={Home} />
				<Route path="/gallery" component={Gallery} />
			</main>
		</Router>
	)
}

export default MainRouter
