import React from 'react'
import { shallow } from 'enzyme';
import Gallery from './Gallery';

describe('testing with empty gallery', () => {
	test('should render message', () => {
		const wrapper = shallow( <Gallery /> );

		expect(wrapper.state('gallery').length).toBe(0);
		expect(wrapper.find('h2').text()).toBe('Use the editor to create your first rect!');
	})
});

describe('testing with gallery', () => {
	const gallery = [
		{ color: "#f78da7", width: "170", height: "250", radius: "11" },
		{ color: "#9900ef", width: "25", height: "25", radius: "41" },
		{ color: "#7bdcb5", width: "250", height: "183", radius: "16" }
	];

	test('should render components', () => {
		const wrapper = shallow( <Gallery /> );
		wrapper.setState({ gallery });

		// make sure there are same number of rects as items in gallery
		expect(wrapper.find('.rect').length).toEqual(gallery.length);
		// check if every style prop matches
		wrapper.find('.rect').forEach((node, i) => {
			expect(node.prop('style').backgroundColor).toEqual(gallery[i].color);
			expect(node.prop('style').width).toEqual(`${gallery[i].width}px`);
			expect(node.prop('style').height).toEqual(`${gallery[i].height}px`);
			expect(node.prop('style').borderRadius).toEqual(`${gallery[i].radius}%`);
		});
	});

	test('removes item from gallery', () => {
		const wrapper = shallow(<Gallery />);
		wrapper.setState({ gallery });
		const rect = wrapper.find('.rect').first();

		rect.simulate('click');
		expect(wrapper.state('gallery').length).toEqual(gallery.length - 1);
	});

	describe('sorting array', () => {
		const wrapper = shallow(<Gallery />);
		wrapper.setState({gallery});

		test('sort array by width', () => {
			wrapper.instance()._handleSelect({target: 'width'});
			expect(wrapper.state('gallery')).toEqual(gallery.sort((a, b) => a.width - b.width));
		});

		test('sort array by height', () => {
			wrapper.instance()._handleSelect({target: 'height'});
			expect(wrapper.state('gallery')).toEqual(gallery.sort((a, b) => a.height - b.height));
		});
	});
});
