import React from 'react';
import { shallow } from 'enzyme';
import Home from './Home';

describe('test if state updates correctly', () => {
	const wrapper = shallow(<Home />);

	test('update width', () => {
		wrapper.instance()._updateState('width', {target: {value: '100'}});
		expect(wrapper.state('width')).toBe('100');
	});
	test('update height', () => {
		wrapper.instance()._updateState('height', {target: {value: '75'}});
		expect(wrapper.state('height')).toBe('75');
	});
	test('update color', () => {
		wrapper.instance()._updateColor({hex: '#9900ef'});
		expect(wrapper.state('color')).toBe('#9900ef');
	});
});
