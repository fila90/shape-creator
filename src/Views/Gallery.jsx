import React from 'react';
import { Link } from 'react-router-dom';
import Nav from '../components/Nav';
import Sorter from '../components/Sorter';

export default class Gallery extends React.Component {
	state = {
		gallery: localStorage.getItem('gallery')
			? JSON.parse(localStorage.getItem('gallery'))
			: [],
	}

	render() {
		const { gallery } = this.state;
		return (
			<div className="container">
				<Nav>
					<Link to="/">Home</Link>
					<Sorter handleSelect={this._handleSelect} />
				</Nav>

				<div>
					{!gallery.length
						?	<h2>Use the editor to create your first rect!</h2>
						: gallery.map((rec, i) => (
							<div
                className="rect"
                key={i}
                onClick={this._handleRemoveItem.bind(this, i)}
                style={{
                  width: `${rec.width}px`,
                  height: `${rec.height}px`,
                  backgroundColor: `${rec.color}`,
                  borderRadius: `${rec.radius}%`,
                  animationDelay: `${0.2 * i}s`,
								}}>
							</div>
					))}
				</div>
			</div>
		)
	}

	/**
	 * @name _handleRemoveItem
	 * @desc remove rect from the list
	 * @param {string}
	 */
	_handleRemoveItem = (i) => {
		const gallery = [...this.state.gallery];
		gallery.splice(i, 1);

		localStorage.setItem('gallery', JSON.stringify(gallery));
		this.setState({ gallery });
	}

	/**
	 * @name _handleSelect
	 * @desc handle sorter select change
	 * @param {event}
	 */
	_handleSelect = (e) => {
		if (!e.target.value) return

		const gallery = [...this.state.gallery];
		if (e.target.value === 'width') {
			gallery.sort((a, b) => a.width - b.width);
		} else if (e.target.value === 'height') {
			gallery.sort((a, b) => a.height - b.height);
		}

		this.setState({ gallery });
	}
}
