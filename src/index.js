import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import MainRouter from './router';
import './styles/index.css';

ReactDOM.render(<MainRouter />, document.getElementById('root'));
registerServiceWorker();
